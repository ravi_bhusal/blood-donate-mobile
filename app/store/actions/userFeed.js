import axios from "axios";
import * as actionTypes from "./actionTypes";

axios.defaults.baseURL = "http://192.168.0.104:8000";

const getFeedUsersListStart = () => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_START
  };
};

const getFeedUsersListSuccess = feedStatuses => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_SUCCESS,
    feedStatuses
  };
};
const getFeedUsersListFail = error => {
  return {
    type: actionTypes.GET_FEED_USERS_LIST_SUCCESS,
    error
  };
};

export const getFeedUserList = (
  token,
  statusType,
  bloodType,
  radius,
  numberOfScrolls
) => {
  return async dispatch => {
    dispatch(getFeedUsersListStart());

    if (statusType === "inNeed") {
      try {
        const feedStatuses = await fetchInNeedStatuses(
          bloodType,
          radius,
          numberOfScrolls
        );
        dispatch(getFeedUsersListSuccess(feedStatuses));
      } catch (error) {
        dispatch(getFeedUsersListFail(error));

        // throw error;
      }
    } else if (statusType === "available") {
      console.log(token);
      try {
        const feedStatuses = await fetchAvailableStatuses(
          bloodType,
          radius,
          numberOfScrolls
        );

        dispatch(getFeedUsersListSuccess(feedStatuses));
      } catch (error) {
        dispatch(getFeedUsersListFail(error));

        //throw error;
      }
    }
  };
};

const fetchAvailableStatuses = async (bloodType, radius, numberOfScrolls) => {
  const res = await axios.get(`/api/availableusers/?bloodType=${bloodType}`, {
    params: {}
  });
  console.log(res.data);
  return res.data;
};

const fetchInNeedStatuses = async (bloodType, radius, numberOfScrolls) => {
  const statusList = await statusListFetch(bloodType, radius, numberOfScrolls);

  const userIdList = Array.from(new Set(statusList.map(({ user }) => user)));

  const userMap = await userMapFetchByIdList(userIdList);

  const statuses = statusList.map(doc => {
    const userFetched = userMap[doc.user] || `USER NOT FOUND`;

    return {
      ...doc,
      userFetched
    };
  });

  return statuses;
};

const statusListFetch = async (bloodType, radius, numberOfScrolls) => {
  try {
    const { data } = await axios.get(`/api/status/?bloodType=${bloodType}`, {
      params: {}
    });

    return data;
  } catch (error) {
    throw error;
  }
};

const userMapFetchByIdList = async idList => {
  const userList = await Promise.all(idList.map(id => userFetch({ id })));

  return userList.reduce((acc, doc) => {
    if (doc) {
      acc[doc.id] = doc;
    }

    return acc;
  }, {});
};

const userFetch = async ({ id }) => {
  try {
    const { data } = await axios.get(`/api/user/${id}/`);

    return data;
  } catch (error) {
    throw error;
  }
};
