import axios from "axios";
import * as actionTypes from "./actionTypes";

export const getCurrentUserStart = () => {
  return {
    type: actionTypes.GET_CURRENT_USER_START
  };
};
export const getCurrentUserSuccess = user => {
  return {
    type: actionTypes.GET_CURRENT_USER_SUCCESS,
    user
  };
};
export const getCurrentUserFail = error => {
  return {
    type: actionTypes.GET_CURRENT_USER_FAIL,
    error
  };
};

export const setAvailableStart = () => {
  return {
    type: actionTypes.SET_AVAILABLE_START
  };
};

export const setAvailableSuccess = availability => {
  return {
    type: actionTypes.SET_AVAILABLE_SUCCESS,
    availability
  };
};
export const setAvailableFail = error => {
  return {
    type: actionTypes.SET_AVAILABLE_FAIL,
    error
  };
};

export const setInNeedStart = () => {
  return {
    type: actionTypes.SET_INNEED_START
  };
};
export const setInNeedSuccess = () => {
  return {
    type: actionTypes.SET_INNEED_SUCCESS
  };
};
export const setInNeedFail = error => {
  return {
    type: actionTypes.SET_INNEED_FAIL,
    error
  };
};

export const setAvailable = (token, postObj) => {
  return dispatch => {
    dispatch(setAvailableStart());

    axios
      .patch(`/api/currentuser/`, postObj)
      .then(res => {
        console.log(res.data.availability);

        dispatch(setAvailableSuccess(res.data.availability));
      })
      .catch(err => {
        dispatch(setAvailableFail(err));
      });
  };
};

export const setInNeed = (token, postObj) => {
  return dispatch => {
    dispatch(setInNeedStart());

    axios
      .post(`/api/status/create/`, postObj)
      .then(res => {
        console.log(res.data);
        dispatch(setInNeedSuccess());
      })
      .catch(err => {
        dispatch(setInNeedFail(err));
      });
  };
};
