import axios from "axios";
import * as actionTypes from "./actionTypes";
import * as actions from "./user";
import { NavigationActions } from "react-navigation";
import RNSecureKeyStore, { ACCESSIBLE } from "react-native-secure-key-store";

axios.defaults.baseURL = "http://192.168.0.104:8000";
export const authStart = () => {
  return {
    type: actionTypes.AUTH_START
  };
};

export const authSuccess = (accessToken, refreshToken) => {
  return {
    type: actionTypes.AUTH_SUCCESS,
    accessToken,
    refreshToken
  };
};

export const authFail = error => {
  return {
    type: actionTypes.AUTH_FAIL,
    error: error
  };
};

export const logout = () => {
  RNSecureKeyStore.remove("accessToken").then(
    res => {
      console.log(res);
    },
    err => {
      console.log(err);
    }
  );

  RNSecureKeyStore.remove("refreshToken").then(
    res => {
      console.log(res);
    },
    err => {
      console.log(err);
    }
  );
  return {
    type: actionTypes.AUTH_LOGOUT
  };
};

export const authLogin = (username, password) => {
  return async dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());

    axios
      .post(
        `/api/login/`,

        {
          username: username,
          password: password
        },
        {
          headers: {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json;charset=utf-8"
            //Authorization: ""
          }
        }
      )
      .then(res => {
        const accessToken = res.data.tokens.access;
        const refreshToken = res.data.tokens.refresh;

        const user = {
          username: res.data.username,
          id: res.data.id,
          first_name: res.data.first_name,
          last_name: res.data.last_name,
          last_login: res.data.last_login,
          bloodtype: res.data.bloodtype,
          phone_number: res.data.phone_number,
          availability: res.data.availability,
          latitude: res.data.latitude,
          longitude: res.data.longitude,
          display_number: res.data.display_number,
          profile_image: res.data.profile_image
        };

        storeNewAccessToken(accessToken);
        storeNewRefreshToken(refreshToken);

        dispatch(authSuccess(accessToken, refreshToken));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
      })
      .catch(error => {
        console.log(error.request);
        dispatch(authFail(error));
      });
  };
};

export const authSignup = (username, email, password1, password2) => {
  return async dispatch => {
    dispatch(authStart());
    dispatch(actions.getCurrentUserStart());
    var instance = axios.create({
      baseURL: "http://192.168.0.104:8000",
      timeout: 1000
    });
    instance
      .post(`/api/register/`, {
        username: username,
        email: email,
        password1: password1,
        password2: password2
      })
      .then(res => {
        const accessToken = res.data.tokens.access;
        const refreshToken = res.data.tokens.refresh;

        const user = {
          username: res.data.username,
          id: res.data.id,
          first_name: res.data.first_name,
          last_name: res.data.last_name,
          last_login: res.data.last_login,
          bloodtype: res.data.bloodtype,
          phone_number: res.data.phone_number,
          availability: res.data.availability,
          latitude: res.data.latitude,
          longitude: res.data.longitude,
          display_number: res.data.display_number,
          profile_image: res.data.profile_image
        };
        storeNewAccessToken(accessToken);
        storeNewRefreshToken(refreshToken);

        dispatch(authSuccess(accessToken, refreshToken));
        if (user !== null || user !== undefined) {
          dispatch(actions.getCurrentUserSuccess(user));
        } else {
          dispatch(actions.getCurrentUserFail("Can't Fetch User data"));
        }
      })
      .catch(error => {
        console.log(error.request);
        dispatch(authFail(error));
      });
  };
};

export const authCheckState = () => {
  return async dispatch => {
    const accessToken = await retrieveAccessToken();
    const refreshToken = await retrieveRefreshToken();

    console.log("refreshToken", refreshToken);
    console.log("accessToken", accessToken);

    // const accessTokenExpirationDate=
    if (accessToken === undefined || accessToken === null) {
      dispatch(actions.authFail("token not valid"));
      dispatch(logout());
      console.log("null token");
    } else {
      const accessValidityStatus = await checkAccessTokenValidity(accessToken);

      // console.log(accessValidityStatus);
      if (accessValidityStatus !== undefined) {
        console.log("fuck");
        dispatch(authSuccess(accessToken, refreshToken));
        dispatch(actions.getCurrentUserStart());
        const currentuser = await getCurrentUser(accessToken);
        console.log(currentuser);
        if (currentuser !== undefined || currentuser !== null) {
          dispatch(actions.getCurrentUserSuccess(currentuser));
        } else {
          dispatch(actions.getCurrentUserFail("Couldn't fetch user"));
        }
      } else {
        console.log(accessValidityStatus);
        const newAccessToken = await getNewAccessToken(refreshToken);
        console.log(newAccessToken.data.access);
        if (
          newAccessToken.data.access !== undefined ||
          newAccessToken.data.access !== null
        ) {
          await deleteOldAccessToken();
          await storeNewAccessToken(newAccessToken.data.access);

          dispatch(authSuccess(newAccessToken.data.access, refreshToken));
          const currentuser = await getCurrentUser(newAccessToken.data.access);

          if (currentuser !== undefined || currentuser !== null) {
            dispatch(actions.getCurrentUserSuccess(currentuser));
          } else {
            dispatch(actions.getCurrentUserFail("Couldn't fetch user"));
          }
        } else {
          dispatch(authFail("token not valid"));
          dispatch(logout());
        }
      }
    }
  };
};

const retrieveRefreshToken = async () => {
  const res = await RNSecureKeyStore.get("refreshToken");
  return res;
};

const retrieveAccessToken = async () => {
  const res = RNSecureKeyStore.get("accessToken");
  return res;
};
const checkAccessTokenValidity = accessToken => {
  var instance = axios.create({
    baseURL: "http://192.168.0.104:8000",
    timeout: 1000,
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json;charset=utf-8"
    }
  });
  //delete axios.defaults.headers.common["Authorization"];
  const res = instance
    .post(`/api/login/verify/`, {
      token: accessToken
    })
    .catch(err => {
      //    throw err;
    });
  return res;
};
const getNewAccessToken = refreshToken => {
  const res = axios
    .post(`/api/login/refresh/`, {
      refresh: refreshToken
    })
    .catch(error => console.log(error.request));
  return res;
};

export const getCurrentUser = async accessToken => {
  const { data } = await axios.get(`/api/currentuser/`, {
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json;charset=utf-8",
      Authorization: `Bearer ${accessToken}`
    }
  });
  return data;
};

export const deleteOldAccessToken = async () => {
  await RNSecureKeyStore.remove("accessToken").then(
    res => {
      console.log(res);
    },
    err => {
      console.log(err);
    }
  );
};
export const storeNewAccessToken = async accessToken => {
  await RNSecureKeyStore.set("accessToken", accessToken, {
    accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY
  }).then(
    res => {
      console.log(res);
    },
    err => {
      console.log(err);
    }
  );
};

export const storeNewRefreshToken = async refreshToken => {
  await RNSecureKeyStore.set("refreshToken", refreshToken, {
    accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY
  }).then(
    res => {
      console.log(res);
    },
    err => {
      console.log(err);
    }
  );
};
