import * as actionTypes from "./actionTypes";

const getLocationStart = () => {
  return {
    type: actionTypes.GET_LOCATION_START
  };
};

const getLocationSuccess = (latitude, longitude, accuracy) => {
  return {
    type: actionTypes.GET_LOCATION_SUCCESS,
    latitude,
    longitude,
    accuracy
  };
};

const getLocationFail = error => {
  return {
    type: actionTypes.GET_LOCATION_FAIL,
    error
  };
};

export const getCurrentLocation = () => {
  return dispatch => {
    dispatch(getLocationStart());
    const geolocation = navigator.geolocation;
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    if (!geolocation) {
      dispatch(getLocationFail("Not Supported"));
    }

    geolocation.getCurrentPosition(
      position => {
        dispatch(
          getLocationSuccess(
            position.coords.latitude,
            position.coords.longitude,
            position.coords.accuracy
          )
        );
      },
      () => {
        dispatch(getLocationFail("Permission denied"));
      },
      options
    );
  };
};
