import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  isFetching: null,
  feedStatuses: undefined,
  userFeedError: null,
  loading: false
};

const getFeedUsersListStart = (state, action) => {
  return updateObject(state, {
    isFetching: true,
    userFeedError: null,
    loading: true
  });
};

const getFeedUsersListSuccess = (state, action) => {
  return updateObject(state, {
    isFetching: false,
    feedStatuses: action.feedStatuses,
    userFeedError: null,
    loading: false
  });
};
const getFeedUsersListFail = (state, action) => {
  return updateObject(state, {
    isFetching: false,
    userFeedError: action.error,
    loading: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_FEED_USERS_LIST_START:
      return getFeedUsersListStart(state, action);
    case actionTypes.GET_FEED_USERS_LIST_SUCCESS:
      return getFeedUsersListSuccess(state, action);
    case actionTypes.GET_FEED_USERS_LIST_FAIL:
      return getFeedUsersListFail(state, action);

    default:
      return state;
  }
};

export default reducer;
