import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  userError: null,
  loading: false,
  username: null,
  first_name: null,
  last_name: null,
  last_login: null,
  userId: null,
  bloodtype: null,
  phone_number: null,
  availability: null,
  latitude: null,
  longitude: null,
  display_number: null,
  profile_image: null
};

const getCurrentUserStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const getCurrentUserSuccess = (state, action) => {
  return updateObject(state, {
    username: action.user.username,
    userId: action.user.id,
    first_name: action.user.first_name,
    last_name: action.user.last_name,
    last_login: action.user.last_login,
    bloodtype: action.user.bloodtype,
    phone_number: action.user.phone_number,
    availability: action.user.availability,
    latitude: action.user.latitude,
    longitude: action.user.longitude,
    display_number: action.user.display_number,
    profile_image: action.user.profile_image,
    userError: null,
    loading: true
  });
};

const getCurrentUserFail = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const setAvailableStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const setAvailableSuccess = (state, action) => {
  return updateObject(state, {
    ...state,
    availability: action.availability,
    userError: null,
    loading: false
  });
};

const setAvailableFail = (state, action) => {
  return updateObject(state, {
    userError: action.error,
    loading: false
  });
};

const setInNeedStart = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: true
  });
};

const setInNeedSuccess = (state, action) => {
  return updateObject(state, {
    userError: null,
    loading: false
  });
};

const setInNeedFail = (state, action) => {
  return updateObject(state, {
    userError: action.error,
    loading: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CURRENT_USER_START:
      return getCurrentUserStart(state, action);
    case actionTypes.GET_CURRENT_USER_SUCCESS:
      return getCurrentUserSuccess(state, action);
    case actionTypes.GET_CURRENT_USER_FAIL:
      return getCurrentUserFail(state, action);

    case actionTypes.SET_INNEED_START:
      return setInNeedStart(state, action);
    case actionTypes.SET_INNEED_SUCCESS:
      return setInNeedSuccess(state, action);
    case actionTypes.SET_INNEED_FAIL:
      return setInNeedFail(state, action);

    case actionTypes.SET_AVAILABLE_START:
      return setAvailableStart(state, action);
    case actionTypes.SET_AVAILABLE_SUCCESS:
      return setAvailableSuccess(state, action);
    case actionTypes.SET_AVAILABLE_FAIL:
      return setAvailableFail(state, action);

    default:
      return state;
  }
};

export default reducer;
