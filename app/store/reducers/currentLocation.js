import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  currentLatitude: null,
  currentLongitude: null,
  accuracy: null,
  locationError: null,
  loading: false
};

const getLocationStart = (state, action) => {
  return updateObject(state, {
    locationError: null,
    loading: true
  });
};

const getLocationSuccess = (state, action) => {
  return updateObject(state, {
    currentLatitude: action.latitude,
    currentLongitude: action.longitude,
    accuracy: action.accuracy,
    locationError: null,
    loading: false
  });
};

const getLocationFail = (state, action) => {
  return updateObject(state, {
    locationError: action.error,
    loading: false
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_LOCATION_START:
      return getLocationStart(state, action);
    case actionTypes.GET_LOCATION_SUCCESS:
      return getLocationSuccess(state, action);
    case actionTypes.GET_LOCATION_FAIL:
      return getLocationFail(state, action);
    default:
      return state;
  }
};
export default reducer;
