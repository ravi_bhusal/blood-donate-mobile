import React from "react";
import { View, Text, Image, TouchableWithoutFeedback } from "react-native";
import LoginPage from "./LoginPage";
import SignupPage from "./SignupPage";
import ProfilePage from "./ProfilePage";
import PostStatusPage from "./PostStatusPage";

import TabNavigator from "../components/TabNavigator";

import {
  createSwitchNavigator,
  createAppContainer,
  createStackNavigator,
  createDrawerNavigator
} from "react-navigation";
import LoggedOutPage from "./LoggedOutPage";

const AuthStack = createStackNavigator(
  {
    LoggedOut: {
      screen: LoggedOutPage
    },
    Login: {
      screen: LoginPage
    },
    Signup: {
      screen: SignupPage
    }
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    },
    initialRouteName: "LoggedOut"
  }
);

const MainStack = createStackNavigator(
  {
    MainHome: {
      screen: TabNavigator,

      navigationOptions: ({ navigation }) => {
        return {
          title:
            navigation.state.routes[navigation.state.index]["routes"][
              navigation.state.routes[navigation.state.index]["index"]
            ].routeName,
          headerLeft: (
            <TouchableWithoutFeedback onPress={() => navigation.toggleDrawer()}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  marginLeft: 10,
                  paddingTop: 5,
                  borderRadius: 100
                }}
                source={{
                  uri:
                    "https://facebook.github.io/react-native/docs/assets/favicon.png"
                }}
              />
            </TouchableWithoutFeedback>
          ),
          headerStyle: {
            elevation: 0
          }
        };
      }
    },
    Profile: {
      screen: ProfilePage,
      navigationOptions: ({ navigation }) => {
        return {
          title: "Profile"
        };
      }
    },
    PostStatus: {
      screen: PostStatusPage,
      navigationOptions: ({ navigation }) => {
        return {
          title: "Post"
        };
      }
    }
  },

  {
    initialRouteName: "MainHome"
  }
);
MainStack.navigationOptions = ({ navigation }) => {
  let drawerLockMode = "unlocked";
  if (navigation.state.index > 0) {
    drawerLockMode = "locked-closed";
  }

  return {
    drawerLockMode
  };
};

const MainDrawerNav = createDrawerNavigator({
  Main: {
    screen: MainStack
  },
  Profile: {
    screen: ProfilePage,
    navigationOptions: ({ navigation }) => {
      return {
        title: "Profile"
      };
    }
  }
});

const AuthNavigator = isAuthenticated => {
  return createAppContainer(
    createSwitchNavigator(
      {
        MainDrawerNav,
        AuthStack
      },
      {
        initialRouteName: isAuthenticated ? "MainDrawerNav" : "AuthStack"
      }
    )
  );
};
export default AuthNavigator;
