import React from "react";
import { View, Text, ScrollView, ActivityIndicator } from "react-native";
import Filter from "../components/Filter";
import FeedStatus from "../components/FeedStatus";
import { connect } from "react-redux";
import * as actions from "../store/actions/userFeed";
import { Button, Icon } from "react-native-elements";

class FeedsScreen extends React.Component {
  state = {
    statusType: "available",
    bloodType: "a",
    radius: "30km",
    numberOfScrolls: 0
  };

  componentDidMount() {
    console.log("FeedScreen");
    if (this.props.token !== undefined && this.props.token !== null) {
      this.props.getFeedUsersList(
        this.props.token,
        this.state.statusType,
        this.state.bloodType,
        this.state.radius,
        this.state.numberOfScrolls
      );
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.token !== this.props.token) {
      if (nextProps.token !== undefined && nextProps.token !== null) {
        this.props.getFeedUsersList(
          nextProps.token,
          this.state.statusType,
          this.state.bloodType,
          this.state.radius,
          this.state.numberOfScrolls
        );
      }
    }
  }

  handleFilterChange = (statusType, bloodType, radius) => {
    console.log(statusType, bloodType, radius);
    this.setState({
      statusType: statusType,
      bloodType: bloodType,
      radius: radius
    });
    if (this.props.token !== undefined && this.props.token !== null) {
      this.props.getFeedUsersList(
        this.props.token,
        statusType,
        bloodType,
        radius,
        this.state.numberOfScrolls
      );
    }
  };

  handleCreatePost = () => {
    this.props.navigation.navigate("PostStatus");
    console.log("clicked");
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView>
          {!this.props.isFetching ? (
            <FeedStatus
              data={this.props.statuses}
              statusType={this.state.statusType}
            />
          ) : (
            <ActivityIndicator size="large" color="#bb0a1e" />
          )}
        </ScrollView>
        <Button
          icon={<Icon name="create" size={20} color="white" />}
          buttonStyle={{
            width: 60,
            height: 60,
            borderRadius: 30,
            backgroundColor: "#b10a1e",
            position: "absolute",
            bottom: 10,
            right: 10
          }}
          title=""
          onPress={this.handleCreatePost}
        />
        <View>
          <Filter onChange={this.handleFilterChange.bind(this)} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.authState.accessToken,
    statuses: state.userFeedState.feedStatuses,
    isFetching: state.userFeedState.isFetching,
    loading: state.userFeedState.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFeedUsersList: (token, statusType, bloodType, radius, numberOfScrolls) =>
      dispatch(
        actions.getFeedUserList(
          token,
          statusType,
          bloodType,
          radius,
          numberOfScrolls
        )
      )
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedsScreen);
