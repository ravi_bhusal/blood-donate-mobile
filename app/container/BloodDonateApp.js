import React from "react";
import { View, ActivityIndicator, Text } from "react-native";
import AuthNavigator from "./AuthNavigator";
import { connect } from "react-redux";
import * as actions from "../store/actions/auth";

class BloodDonateApp extends React.Component {
  state = {};

  /*  componentDidMount() {
    this.props.onTryAutoSignup();
  } */

  componentDidMount = () => {
    //retrieveToken();
    this.props.checkTokenValidity();
  };
  render() {
    if (
      this.props.isAuthenticated !== null ||
      this.props.isAuthenticated !== undefined
    ) {
      const AppNavigator = AuthNavigator(this.props.isAuthenticated);

      return <AppNavigator />;
    }
    return (
      <View styles={{ flex: 1, justifyContent: "center" }}>
        <Text>Fuck</Text>
        <ActivityIndicator size="large" color="#bb0a1e" />
      </View>
    );
  }
}

export const retrieveToken = async () => {};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authState.isAuthenticated
  };
};

const mapDispatchToProps = dispatch => {
  return {
    checkTokenValidity: () => {
      dispatch(actions.authCheckState());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BloodDonateApp);
