import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";

import { Input, Button } from "react-native-elements";
import * as actions from "../store/actions/auth";

class SignupPage extends React.Component {
  state = {};

  componentWillReceiveProps(newProps) {
    if (newProps.token !== null || newProps.token !== undefined) {
      this.props.navigation.navigate("MainDrawerNav");
    }
  }

  handleSubmit = event => {
    this.props.onAuth(
      this.state.userName,
      this.state.email,
      this.state.password1,
      this.state.password2
    );
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text>Sign up to Blood Donate</Text>
        <Input
          placeholder="Username"
          leftIcon={{ type: "font-awesome", name: "user" }}
          name="username"
          onChangeText={text => this.setState({ userName: text })}
        />
        <Input
          placeholder="Email"
          leftIcon={{ type: "font-awesome", name: "email" }}
          name="email"
          onChangeText={text => this.setState({ email: text })}
        />
        <Input
          placeholder="Password"
          leftIcon={{ type: "font-awesome", name: "lock" }}
          name="password1"
          onChangeText={text => this.setState({ password1: text })}
        />
        <Input
          placeholder="Confirm Password"
          secureTextEntry={true}
          leftIcon={{ type: "font-awesome", name: "lock" }}
          name="password2"
          onChangeText={text => this.setState({ password2: text })}
        />

        <Button onPress={this.handleSubmit} title="Sign up" />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    token: state.authState.accessToken
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuth: (username, email, password1, password2) => {
      dispatch(actions.authSignup(username, email, password1, password2));
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupPage);
