import React from "react";
import { View, Text } from "react-native";
import { Button, Icon } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";

class MessagesScreen extends React.Component {
  state = {};

  componentDidMount() {}

  handleCreatePost = () => {
    this.props.navigation.navigate("PostStatus");
    console.log("clicked");
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView />

        <Button
          icon={<Icon name="create" size={20} color="white" />}
          buttonStyle={{
            width: 60,
            height: 60,
            borderRadius: 30,
            backgroundColor: "#b10a1e",
            position: "absolute",
            bottom: 10,
            right: 10
          }}
          title=""
          onPress={this.handleCreatePost}
        />
      </View>
    );
  }
}

export default MessagesScreen;
