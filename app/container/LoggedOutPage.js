import React from "react";
import { Button } from "react-native-elements";
import { View } from "react-native";

class LoggedOutPage extends React.Component {
  handleLoginPress = () => {
    this.props.navigation.navigate("Login");
  };

  handleSignupPress = () => {
    this.props.navigation.navigate("Signup");
  };
  render() {
    return (
      <View>
        <Button title="Login" onPress={this.handleLoginPress} />
        <Button title="Signup" onPress={this.handleSignupPress} />
      </View>
    );
  }
}

export default LoggedOutPage;
