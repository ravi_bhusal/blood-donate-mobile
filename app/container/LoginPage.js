import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";

import { Input, Button } from "react-native-elements";
import * as actions from "../store/actions/auth";

class LoginPage extends React.Component {
  state = {};

  componentWillReceiveProps(newProps) {
    if (newProps.token !== null || newProps.token !== undefined) {
      this.props.navigation.navigate("MainDrawerNav");
    }
  }

  handleSubmit = event => {
    this.props.onAuth(this.state.userName, this.state.password);
    console.log(this.state.userName, this.state.password);
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Text>Log in to Blood Donate</Text>
        <Input
          placeholder="Username"
          leftIcon={{ type: "font-awesome", name: "user" }}
          name="username"
          onChangeText={text => this.setState({ userName: text })}
        />
        <Input
          placeholder="Password"
          secureTextEntry={true}
          leftIcon={{ type: "font-awesome", name: "lock" }}
          name="password"
          onChangeText={text => this.setState({ password: text })}
        />

        <Button onPress={this.handleSubmit} title="Login" />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.authState.accessToken
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onAuth: (username, password) => {
      dispatch(actions.authLogin(username, password));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);
