import React, { Component } from "react";
import { Provider } from "react-redux";

import configureStore from "../store/configuration/index";
import BloodDonateApp from "./BloodDonateApp";

const store = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BloodDonateApp />
      </Provider>
    );
  }
}
