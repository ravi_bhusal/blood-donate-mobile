import React from "react";
import moment from "moment";
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableWithoutFeedback
} from "react-native";
import { Button, Icon } from "react-native-elements";

const AvailableStatus = props => {
  console.log("Available", props.data);
  if (props.data.length !== 0) {
    console.log("not null");
    return (
      <View>
        <FlatList
          data={props.data}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({ item }) => (
            <View
              style={{
                margin: 5,
                flex: 1,
                borderRadius: 1,
                borderBottomWidth: 0.5
              }}
            >
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <Image
                  style={{
                    width: 40,
                    height: 40,

                    paddingTop: 5,
                    borderRadius: 100
                  }}
                  source={{
                    uri:
                      "https://facebook.github.io/react-native/docs/assets/favicon.png"
                  }}
                />
                <View
                  style={{
                    marginLeft: 5,
                    flex: 1,
                    alignSelf: "flex-start"
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-end"
                    }}
                  >
                    <Text style={{ fontWeight: "bold", color: "black" }}>
                      {item.first_name} {item.last_name}
                    </Text>

                    <Text> has </Text>
                    <Text style={{ fontWeight: "bold", color: "#b10a1e" }}>
                      {`${item.bloodtype}`.match("-")
                        ? `${item.bloodtype}`.toUpperCase()
                        : `${item.bloodtype}`.toUpperCase() + "+"}
                    </Text>

                    <Text
                      style={{
                        marginLeft: "auto",
                        alignSelf: "flex-end"
                      }}
                    >
                      {moment(item.last_login)
                        .startOf("hour")
                        .fromNow()}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text>near </Text>
                    <Text style={{ fontWeight: "bold", color: "#b10a1e" }}>
                      100m,
                    </Text>
                    <Icon name="phone" size={15} marginLeft={5} marginTop={2} />
                    <Text>{item.phone_number}</Text>
                  </View>
                </View>
              </View>
              <Text style={{ marginLeft: 45, marginTop: 5 }}>
                Hi! I am available to donate.
              </Text>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  marginLeft: 45,
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                <TouchableWithoutFeedback>
                  <Icon name="message" size={18} color="#b10a1e" />
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                  <Icon
                    name="phone"
                    size={18}
                    marginLeft={20}
                    color="#b10a1e"
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>
          )}
        />
      </View>
    );
  } else {
    console.log(" null");

    return <Text>Looks like no one is available right now :(. </Text>;
  }
};

const InNeedStatus = props => {
  console.log("In need", props.data);

  if (props.data.length !== 0) {
    return (
      <View>
        <FlatList
          data={props.data}
          keyExtractor={(item, index) => `${item.id}`}
          renderItem={({ item }) => (
            <View
              style={{
                margin: 5,
                flex: 1,
                borderRadius: 1,
                borderBottomWidth: 0.5
              }}
            >
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <Image
                  style={{
                    width: 40,
                    height: 40,

                    paddingTop: 5,
                    borderRadius: 100
                  }}
                  source={{
                    uri:
                      "https://facebook.github.io/react-native/docs/assets/favicon.png"
                  }}
                />
                <View
                  style={{
                    marginLeft: 5,
                    flex: 1,
                    alignSelf: "flex-start"
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "flex-end"
                    }}
                  >
                    <Text style={{ fontWeight: "bold", color: "black" }}>
                      Ravi Bhusal
                    </Text>

                    <Text> needs </Text>
                    <Text style={{ fontWeight: "bold", color: "#b10a1e" }}>
                      {`${item.required_bloodtype}`.match("-")
                        ? `${item.required_bloodtype}`.toUpperCase()
                        : `${item.required_bloodtype}`.toUpperCase() + "+"}{" "}
                      ({item.number_of_pints_required} pints)
                    </Text>

                    <Text
                      style={{
                        marginLeft: "auto",
                        alignSelf: "flex-end"
                      }}
                    >
                      {moment(item.posted_time)
                        .startOf("hour")
                        .fromNow()}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text>near </Text>
                    <Text style={{ fontWeight: "bold", color: "#b10a1e" }}>
                      100m,
                    </Text>
                    <Icon name="phone" size={15} marginLeft={5} marginTop={2} />
                    <Text>{item.userFetched.phone_number}</Text>
                  </View>
                </View>
              </View>
              <Text style={{ marginLeft: 45, marginTop: 5 }}>
                {item.extra_info}
              </Text>

              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  marginLeft: 45,
                  marginTop: 10,
                  marginBottom: 10
                }}
              >
                <TouchableWithoutFeedback>
                  <Icon name="message" size={18} color="#b10a1e" />
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback>
                  <Icon
                    name="phone"
                    size={18}
                    marginLeft={20}
                    color="#b10a1e"
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>
          )}
        />
      </View>
    );
  } else {
    return (
      <Text style={{ fontWeight: "bold" }}>
        Looks like no one is in need of blood right now :).{" "}
      </Text>
    );
  }
};

class FeedStatus extends React.Component {
  state = {};

  render() {
    console.log("hellow!!!", this.props.data);
    if (
      this.props.statusType === "available" &&
      this.props.data !== undefined
    ) {
      return <AvailableStatus data={this.props.data} />;
    } else if (
      this.props.statusType === "inNeed" &&
      this.props.data !== undefined
    ) {
      return <InNeedStatus data={this.props.data} />;
    } else if (this.props.data === undefined) {
      return (
        <Text>
          Sorry there is problem fetching the data. Check your internet
          connection or try again later.
        </Text>
      );
    }
  }
}

export default FeedStatus;
