import React from "react";

import { View, Picker } from "react-native";

class Filter extends React.Component {
  state = {
    bloodType: "a",
    statusType: "available",
    radius: "30km"
  };

  handleTypeChange = (itemValue, itemIndex) => {
    this.setState({ statusType: itemValue });
    this.props.onChange(itemValue, this.state.bloodType, this.state.radius);
  };

  handleBloodTypeChange = (itemValue, itemIndex) => {
    this.setState({ bloodType: itemValue });
    this.props.onChange(this.state.statusType, itemValue, this.state.radius);
  };
  handleRadiusChange = (itemValue, itemIndex) => {
    this.setState({ radius: itemValue });
    this.props.onChange(this.state.statusType, this.state.bloodType, itemValue);
  };
  render() {
    return (
      <View>
        <View
          style={{
            borderTopWidth: 0.25,
            backgroundColor: "#fff",
            height: 50,
            flexDirection: "row"
          }}
        >
          <Picker
            selectedValue={this.state.statusType}
            style={{ height: 40, flex: 0.45, marginLeft: 10 }}
            onValueChange={this.handleTypeChange}
          >
            <Picker.Item label="Available" value="available" />
            <Picker.Item label="In Need" value="inNeed" />
          </Picker>

          <Picker
            selectedValue={this.state.bloodType}
            style={{ height: 40, flex: 0.3, marginLeft: 0 }}
            onValueChange={this.handleBloodTypeChange}
          >
            <Picker.Item label="A+" value="a" />
            <Picker.Item label="A-" value="a-" />
            <Picker.Item label="B+" value="b" />
            <Picker.Item label="B-" value="b-" />
            <Picker.Item label="AB+" value="ab" />
            <Picker.Item label="AB-" value="ab-" />
            <Picker.Item label="O+" value="o" />
            <Picker.Item label="O-" value="o-" />
          </Picker>

          <Picker
            selectedValue={this.state.radius}
            style={{ height: 40, flex: 0.35, marginLeft: 0 }}
            onValueChange={this.handleRadiusChange}
          >
            <Picker.Item label="5 K m" value="5km" />
            <Picker.Item label="10 Km" value="10km" />
            <Picker.Item label="30 Km" value="30km" />
            <Picker.Item label="Global" value="" />
          </Picker>
        </View>
      </View>
    );
  }
}

export default Filter;
