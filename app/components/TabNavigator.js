import React from "react";
import { View, Text } from "react-native";
import {
  createStackNavigator,
  createAppContainer,
  createMaterialTopTabNavigator
} from "react-navigation";
import { Icon } from "react-native-elements";
import FeedsScreen from "../container/FeedsScreen";
import NotificationsScreen from "../container/NotificationsScreen";
import MessagesScreen from "../container/MessagesScreen";

const Home = createStackNavigator({
  Home: {
    screen: FeedsScreen,
    navigationOptions: {
      header: null //Need to set header as null.
    }
  }
});

//Screen2 Stack

const Notifications = createStackNavigator({
  Notifications: {
    screen: NotificationsScreen,
    navigationOptions: {
      header: null //Need to set header as null.
    }
  }
});
const Messages = createStackNavigator({
  Messages: {
    screen: MessagesScreen,
    navigationOptions: {
      header: null //Need to set header as null.
    }
  }
});

const TabNavigator = createMaterialTopTabNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => {
        return {
          tabBarLabel: "Home"
        };
      }
    },

    Notifications: {
      screen: Notifications,
      navigationOptions: ({ navigation }) => {
        return {
          tabBarLabel: "Notifications"
        };
      }
    },

    Messages: {
      screen: Messages,
      navigationOptions: ({ navigation }) => {
        return {
          tabBarLabel: "Messages"
        };
      }
    }
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) =>
        getTabBarIcon(navigation, focused, tintColor)
    }),
    tabBarOptions: {
      style: {
        backgroundColor: "#fff"
      },
      indicatorStyle: {
        backgroundColor: "#bb0a1e"
      },
      activeTintColor: "#bb0a1e",
      inactiveTintColor: "gray"
    }
  }
);

export default createAppContainer(TabNavigator);
